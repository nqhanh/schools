<?php
// TODO
// - fix TML hack below
// REGISTRATION ERRORS
function tml_registration_errors( $errors ) {
/*
// First Name
	if ( empty( $_POST['first_name'] ) ) {
		$errors->add( 'empty_first_name', '<strong>ERROR</strong>: Please type your first name.' );
	}
	if ( !empty( $_POST['first_name'] ) ) {
		$value_first_name = trim($_POST['first_name']);
		$value_first_name = sanitize_text_field($value_first_name);
		if (strlen($value_first_name) > '16') {
			$errors->add( 'maxlength_first_name', '<strong>ERROR</strong>: Please enter a first name with 16 or fewer characters.' );
		}
		elseif (!preg_match("/^[a-zA-Z \\\'-]+$/", $value_first_name)) {
			$errors->add( 'invalid_first_name', '<strong>ERROR</strong>: Invalid characters in first name. Please enter a first name using only letters, space, hyphen, and apostrophe.' );
		}
	}
// Last Name
/*
	if ( empty( $_POST['last_name'] ) ) {
		$errors->add( 'empty_last_name', '<strong>ERROR</strong>: Please type your last name.' );
	}
	if ( !empty( $_POST['last_name'] ) ) {
		$value_last_name = trim($_POST['last_name']);
		$value_last_name = sanitize_text_field($value_last_name);
		if (strlen($value_last_name) > '16') {
			$errors->add( 'maxlength_last_name', '<strong>ERROR</strong>: Please enter a last name with 16 or fewer characters.' );
		}
		elseif (!preg_match("/^[a-zA-Z \\\'-]+$/", $value_last_name)) {
			$errors->add( 'invalid_last_name', '<strong>ERROR</strong>: Invalid characters in last name. Please enter a last name using only letters, space, hyphen, and apostrophe.' );
		}
	}
	*/
// Username	- from WP - try for a sane min/max length
	// let WP handle validation
	if ( !empty( $_POST['user_login'] ) ) {
		$value_user_login = trim($_POST['user_login']);
		if (strlen($value_user_login) < '6') {
			$errors->add( 'minlength_user_login', '<strong>Lỗi</strong>: Vui lòng nhập tên tài khoản từ 6 ký tự trở lên.' );
			echo '
			<style>
				input#user_login{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
		}
		elseif (strlen($value_user_login) > '16') {
			$errors->add( 'maxlength_user_login', '<strong>Lỗi</strong>: Vui lòng nhập tên tài khoản ít hơn 16 ký tự.' );
			echo '
			<style>
				input#user_login{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
		}
		elseif (!preg_match("/^[_a-zA-Z0-9-]+$/", $value_user_login)) {
			$errors->add( 'invalid_user_login', '<strong>Lỗi</strong>: Vui lòng chỉ tên tài khoản nhập chữ và số.' );
			echo '
			<style>
				input#user_login{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
		}
	}

	if(empty($_POST['user_login'])){
		//$errors->add('empty_user_login','<strong>Lỗi</strong>: Vui lòng nhập tên tài khoản.');
		echo '
			<style>
				input#user_login{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	if(empty($_POST['user_email'])){
		//$errors->add('empty_user_email','<strong>Lỗi</strong>: Vui lòng nhập địa chỉ email.');
		echo '
			<style>
				input#user_email{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	// Make sure passwords match
	if ( $_POST['user_email'] != $_POST['user_email2'] ) {
		$errors->add( 'email_mismatch', __( '<strong>Lỗi</strong>: Vui lòng nhập địa chỉ email chính xác.' ) );
		echo '
			<style>
				input#user_email, input#user_email2{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	

	if(empty($_POST['user_ht'])){
		$errors->add('user_ht','<strong>Lỗi</strong>: Vui lòng nhập họ và tên.');
		echo '
			<style>
				input#user_ht{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	
	//Day
	if ( empty( $_POST['user_bday'] ) ) {
		$errors->add('user_bday','<strong>Lỗi</strong>: Vui lòng nhập ngày sinh.');
		echo '
			<style>
				input#user_bday{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	
	if(empty($_POST['user_address_dd'])){
		$errors->add('empty_user_address_dd','<strong>Lỗi</strong>: Vui lòng nhập địa chỉ.');
		echo '
			<style>
				input#user_address_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	/*
	if(empty($_POST['user_sex'])){
		$errors->add('user_sex','<strong>Lỗi</strong>: Vui lòng chọn giới tính.');
		echo '
			<style>
				span#user_sex{
					border:1px solid red;"
				}
			</style>
			';
	}*/


	if ( empty( $_POST['user_phone_dd'] )) {
		$errors->add( 'empty_user_phone_dd', '<strong>Lỗi</strong>: Vui lòng nhập số điện thoại.' );
		echo '
			<style>
				input#user_phone_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	else {
		if ( ! is_numeric( $_POST['user_phone_dd'] ) ) {
			$errors->add( 'empty_user_phone_dd', '<strong>Lỗi</strong>: Số điện thoại không hợp lệ.' );
			echo '
			<style>
				input#user_phone_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
		}
	}

	$get_checked = "";

	if (isset($_POST['user_dk_dd'])) {
		if ($_POST['user_dk_dd'] == '1') {
			$subscribed = true;
			$get_checked = "checked='checked'";
		} else {
			$subscribed = false;
		}
	} else {
		$subscribed = false;
	}

	if (!$subscribed) {
		$errors->add( 'empty_user_dk_dd', '<strong>Lỗi</strong>: Vui lòng đồng ý điều khoản của chúng tôi.' );
	}


	return $errors;
}
add_filter( 'registration_errors', 'tml_registration_errors' );

// INSERT VALUES
function tml_user_register( $user_id ) {
// NAMES
	if ( !empty( $_POST['first_name'] ) ) {
		$un_first_name = trim($_POST['first_name']);
		$first_name = sanitize_text_field($un_first_name);
		update_user_meta($user_id, 'first_name', $first_name);
	}

	if ( !empty( $_POST['last_name'] ) ) {
		$un_last_name = trim($_POST['last_name']);
		$last_name = sanitize_text_field($un_last_name);
		update_user_meta($user_id, 'last_name', $last_name);
	}

	if ( !empty( $_POST['first_name'] ) AND !empty( $_POST['last_name'] )) {
		wp_update_user(array(
			'ID' => $user_id,
			'display_name' => $first_name.' '.$last_name
		));
	}



	//	New confirm email
	if ( !empty( $_POST['user_email2'] ) )
		update_user_meta( $user_id, 'user_email2', $_POST['user_email2'] );

		if ( !empty( $_POST['user_email'] ) )
		update_user_meta( $user_id, 'user_email', $_POST['user_email'] );

	//phone
	if ( !empty( $_POST['user_phone_dd'] ) ) {
		update_user_meta( $user_id, 'user_phone_dd', $_POST['user_phone_dd'] );
	}


	//New BDay
	if ( !empty( $_POST['user_bday'] ) )
		update_user_meta( $user_id, 'user_bday', $_POST['user_bday'] );

	//	New gioi tinh
	if ( !empty( $_POST['user_sex'] ) )
		update_user_meta( $user_id, 'user_sex', $_POST['user_sex'] );

	if(!empty($_POST['user_ht']))
		update_user_meta($user_id,'user_ht', $_POST['user_ht']);

	if(!empty($_POST['user_address_dd']))
		update_user_meta($user_id,'user_address_dd', $_POST['user_address_dd']);

	if(!empty($_POST['user_level_dd']))
		update_user_meta($user_id,'user_level_dd', $_POST['user_level_dd']);

	$get_checked = "";

	if (isset($_POST['user_dk_dd'])) {
		if ($_POST['user_dk_dd'] == '1') {
			$subscribed = true;
			$get_checked = "checked='checked'";
		} else {
			$subscribed = false;
		}
	} else {
		$subscribed = false;
	}

	if ($subscribed) {
		update_user_meta($user_id,'user_dk_dd', $_POST['user_dk_dd']);
	}

//Hien thi cac truong Len trang dang ky
	?>
		<p>
			<label for="user_email2"><?php _e('Confirm Email'); ?> <span class="description"><?php _e( ' (*)' ); ?></span></label>
			<input type="text"  style="height:35px;" name="user_email2"  id="user_email2" class="input" value="<?php echo esc_attr( $_POST['user_email2'] ); ?>" size="20" />
		</p>
		<p>
			<label for="user_ht"><?php _e('Họ và tên') ?> <span class="description"><?php _e( ' (*)' ); ?></span></label>
			<input type="text" name="user_ht"  id="user_ht" class="input" value="<?php echo esc_attr(  $_POST['user_ht'] ); ?>" size="20" />
		</p>

		<p>
			<td><label for="user_bday"><?php _e( 'Ngày sinh','theme-my-login' );?></span></label></td>
			<td><input type="date" name="user_bday" max="2050-12-31" id="user_bday" value="<?php echo esc_attr( $user_bday ); ?>" class="regular-text" /></td>
		</p>
		
		<p>
			<label for="user_sex"><?php _e('Giới tính');?></label>
			<span id="user_sex">
				<input type="radio" name="user_sex" value="1" checked="checked"><?php _e( 'Nam', 'theme-my-login' ); ?>&nbsp;&nbsp;
				<input type="radio" name="user_sex" value="2"><?php _e('Nữ', 'theme-my-login' ); ?>
			</span>

		</p>
		<p>
			<label for="user_address_dd"><?php _e('Địa chỉ'); ?> <span class="description"><?php _e( ' (*)' ); ?></span></label>
			<input type="text" name="user_address_dd"  id="user_address_dd" class="input" value="<?php echo esc_attr( $_POST['user_address_dd'] ); ?>" size="20" />
		</p>

			<p>
			<label for="user_phone_dd"><?php _e( 'Số điện thoại');?><span class="description"><?php _e( ' (*)' ); ?></span></label>
			<input type="text"  name="user_phone_dd" id="user_phone_dd" value="<?php echo esc_attr( $_POST['user_phone_dd'] ); ?>" class="regular-text" /><span id="spnPhoneStatus"></span>
		</p>

		<p>
			<label for="user_level_dd"><?php _e('Cấp độ tiếng Nhật'); ?></label>

			<select name="user_level_dd" id="user_level_dd">
				  <option value="1" <?php echo (esc_attr( $_POST['user_level_dd'])=='1')?'selected':''; ?>>N1</option>
				  <option value="2" <?php echo (esc_attr( $_POST['user_level_dd'])=='2')?'selected':''; ?>>N2</option>
				  <option value="3" <?php echo (esc_attr( $_POST['user_level_dd'])=='3')?'selected':''; ?>>N3</option>
				  <option value="4" <?php echo (esc_attr( $_POST['user_level_dd'])=='4')?'selected':''; ?>>N4</option>
				  <option value="5" <?php echo (esc_attr( $_POST['user_level_dd'])=='5')?'selected':''; ?>>N5</option>
				  <option value="0" <?php echo (esc_attr( $_POST['user_level_dd'])=='0')?'selected':''; ?>>Chưa biết tiếng Nhật</option>
				</select>
		</p>

		<p>

			<i><input type="checkbox" name="user_dk_dd" id="user_dk_dd" value='1' <?php echo $get_checked;?> >&nbsp;&nbsp;<a href="<?php echo site_url()?>/noi-quy-clb-dong-du/" target="_blank">Tôi đã xem và đồng ý với các điều khoản của nội quy Câu lạc bộ.</a></i>
		</p>






	<?php
}

add_action('the_action_hook1','tml_user_register');

add_action( 'user_register', 'tml_user_register' );



/*------- UPDATE THESE FIELDS IN ADMIN PROFILE-----*/
// also adding errors for Description to limit length


function nh_save_extra_profile_fields( &$errors, $update, &$user ) {
	if($update) {

// FIRST NAME - required + of right format
	/*
		if(empty($_POST['first_name'])) {
			$errors->add('empty_first_name', '<strong>ERROR</strong>: First name is required. Please type your first name.', array('form-field' => 'first_name'));
		}
		elseif (!empty($_POST['first_name'])) {
			$value_first_name = trim($_POST['first_name']);
			$value_first_name = sanitize_text_field($value_first_name);
			if (strlen($value_first_name) > '16') {
				$errors->add('maxlength_first_name', '<strong>ERROR</strong>: Please enter a first name with 16 or fewer characters.', array('form-field' => 'first_name'));
			}
			if (!preg_match("/^[a-zA-Z \\\'-]+$/", $value_first_name)) {
				$errors->add('invalid_first_name', '<strong>ERROR</strong>: Invalid characters in first name. Please enter a first name using only letters, spaces, hyphens, or apostrophes.', array('form-field' => 'first_name'));
			}
			else {
				update_user_meta($user->ID, 'first_name', $value_first_name);
				$last = get_user_meta($user->ID,'last_name',true);
				wp_update_user(array(
					'ID' => $user->ID,
					'display_name' => $value_first_name.' '.$last
				));
			}
		}

	// LAST NAME - required + of right format
		if(empty($_POST['last_name'])) {
			$errors->add('empty_last_name', '<strong>ERROR</strong>: Last name is required. Please type your last name.', array('form-field' => 'last_name'));
		}
		elseif (!empty($_POST['last_name'])) {
			$value_last_name = trim($_POST['last_name']);
			$value_last_name = sanitize_text_field($value_last_name);
			if (strlen($value_last_name) > '16') {
				$errors->add('maxlength_last_name', '<strong>ERROR</strong>: Please enter a last name with 16 or fewer characters.', array('form-field' => 'last_name'));
			}
			if (!preg_match("/^[a-zA-Z \\\'-]+$/", $value_last_name)) {
				$errors->add('invalid_last_name', '<strong>ERROR</strong>: Invalid characters in last name. Please enter a last name using only letters, spaces, hyphens, or apostrophes.', array('form-field' => 'last_name'));
			}
			else {
				update_user_meta($user->ID, 'last_name', $value_last_name);
				$first = get_user_meta($user->ID,'first_name',true);
				wp_update_user(array(
					'ID' => $user->ID,
					'display_name' => $first.' '.$value_last_name
				));
			}
		}
		/*
	// DESCRIPTION - let WP handle validation
		if (!empty($_POST['description'])) {
			$value_description = trim($_POST['description']);
			if (strlen($value_description) > '200') {
				$errors->add('maxlength_description', '<strong>Lỗi</strong>: Vui lòng nhập mô tả dưới 200 ký tự.', array('form-field' => 'description'));
			}
			else {
				update_user_meta($user->ID, 'description', $value_description);
			}
		}
		*/

	//Kiem tra User Họ Tên
	if ( empty( $_POST['user_ht'] ) ) {
		$errors->add( 'empty_user_ht', __('<strong>Lỗi</strong>: Vui lòng nhập họ và tên.') );	//todo: hntrhau Kiem tra User Họ Tên
		echo '
			<style>
				input#user_ht{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	//Kiem tra User phone
	if ( empty( $_POST['user_phone_dd'] )) {
		$errors->add( 'empty_user_phone_dd', '<strong>Lỗi</strong>: Vui lòng nhập số điện thoại.' );
		echo '
			<style>
				input#user_phone_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	else{
		if(!is_numeric( $_POST['user_phone_dd'] )) {
			$errors->add( 'empty_user_phone_dd', '<strong>Lỗi</strong>: Số điện thoại không hợp lệ.' );
			echo '
			<style>
				input#user_phone_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
		}
	}

	//Kiem tra User address
	if ( empty( $_POST['user_address_tr_dd'] ) ) {
		$errors->add( 'empty_user_address_tr_dd', __('<strong>Lỗi</strong>: Vui lòng nhập hộ khẩu thường trú.') );
		echo '
			<style>
				input#user_address_tr_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	//Kiem tra Nơi ỏ hiện nay
	if ( empty( $_POST['user_address_dd'] ) ) {
		$errors->add( 'empty_user_address_dd', __('<strong>Lỗi</strong>: Vui lòng nhập nơi ở hiện nay.') );
		echo '
			<style>
				input#user_address_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	if ( empty( $_POST['user_slcv_dd'] ) ) {
		$errors->add( 'empty_user_slcv_dd', __('<strong>Lỗi</strong>: Vui lòng nhập kinh nghiệm công việc bạn đã từng làm.') );
		echo '
			<style>
				input#user_slcv_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	//Kiem tra Lý do làm việc
	if ( empty( $_POST['user_ld_dd'] ) ) {
		$errors->add( 'empty_user_ld_dd', __('<strong>Lỗi</strong>: Vui lòng nhập lý do bạn muốn làm việc tại Nhật Bản.') );
		echo '
			<style>
				input#user_ld_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}
	//Kiem tra bạn muốn làm việc gì tại nhật
	if ( empty( $_POST['user_vlnb_dd'] ) ) {
		$errors->add( 'empty_user_vlnb_dd', __('<strong>Lỗi</strong>: Vui lòng nhập công việc bạn muốn làm tại Nhật.') );
		echo '
			<style>
				input#user_vlnb_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}

	//Thoi gian

	if ( empty( $_POST['user_tg_dd'] ) && $_POST['user_di_dd'][1]=='1') {
		$errors->add( 'empty_user_vlnb_dd', __( '<strong>Lỗi</strong>: Vui lòng nhập khoảng thời gian bạn đi Nhật.' ) );
		echo '
			<style>
				input#user_tg_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}


	if ( empty( $_POST['user_tgvn_dd'] ) && $_POST['user_khong_dd'][1]=='1') {
		$errors->add( 'empty_user_vlnb_dd', __( '<strong>Lỗi</strong>: Vui lòng nhập khoảng thời gian bạn làm việc cho công ty Nhật tại Việt Nam.' ) );
		echo '
			<style>
				input#user_tgvn_dd{
					border:1px solid red;
					height: 35px;
				}
			</style>
			';
	}



	$user_id = $user->ID;

	//CAP NHAT PROFILE
	if ( !empty( $_POST['user_phone_dd'] ) ) {
		update_user_meta( $user_id, 'user_phone_dd', $_POST['user_phone_dd'] );
	}

	//Cap nhat BDay
	if ( !empty( $_POST['user_bday'] ) )
		update_user_meta( $user_id, 'user_bday', $_POST['user_bday'] );


	//Cap nhat sex
	if ( !empty( $_POST['user_sex'] ) )
		update_user_meta( $user_id, 'user_sex', $_POST['user_sex'] );

	//Cap nhat address
	if ( !empty( $_POST['user_address_dd'] ) )
		update_user_meta( $user_id, 'user_address_dd', $_POST['user_address_dd'] );

	//Cap nhat address T TRú
	if ( !empty( $_POST['user_address_tr_dd'] ) )
		update_user_meta( $user_id, 'user_address_tr_dd', $_POST['user_address_tr_dd'] );

	//Cap nhat LV
	if ( !empty( $_POST['user_level_dd'] ) )
		update_user_meta( $user_id, 'user_level_dd', $_POST['user_level_dd'] );

	//Cap nhat BC
	if ( !empty( $_POST['user_bc_dd'] ) )
		update_user_meta( $user_id, 'user_bc_dd', $_POST['user_bc_dd'] );

	//Cap nhat BC
	if ( !empty( $_POST['user_ocupation_dd'] ) )
		update_user_meta( $user_id, 'user_ocupation_dd', $_POST['user_ocupation_dd'] );

	//Cap nhat School
	if ( !empty( $_POST['user_school_dd'] ) )
		update_user_meta( $user_id, 'user_school_dd', $_POST['user_school_dd'] );

	//Cap nhat slcv
	if ( !empty( $_POST['user_slcv_dd'] ) )
		update_user_meta( $user_id, 'user_slcv_dd', $_POST['user_slcv_dd'] );
	//Cap nhat Lý do
	if ( !empty( $_POST['user_ld_dd'] ) )
		update_user_meta( $user_id, 'user_ld_dd', $_POST['user_ld_dd'] );
	//Cap nhat việc làm Nhật bản
	if ( !empty( $_POST['user_vlnb_dd'] ) )
		update_user_meta( $user_id, 'user_vlnb_dd', $_POST['user_vlnb_dd'] );

	//Cap nhat Hoc TN
	if ( !empty( $_POST['user_htn_dd'] ) )
		update_user_meta( $user_id, 'user_htn_dd', $_POST['user_htn_dd'] );


	//Cap nhat TL
	if ( !empty( $_POST['user_di_dd'] ) )
	{	if($_POST['user_di_dd'][1]=='1')
			update_user_meta( $user_id, 'user_di_dd', $_POST['user_di_dd'][1] );
		else
			update_user_meta( $user_id, 'user_di_dd', $_POST['user_di_dd'][0] );
	}

	if ( !empty( $_POST['user_khong_dd'] ) )
	{
		if($_POST['user_khong_dd'][1]=='1')
			update_user_meta( $user_id, 'user_khong_dd', $_POST['user_khong_dd'][1] );
		else
			update_user_meta( $user_id, 'user_khong_dd', $_POST['user_khong_dd'][0] );
	}
	//Thoi gian

		if ( !empty( $_POST['user_di_dd']) && !empty( $_POST['user_tg_dd'] ) )
		{
			if($_POST['user_di_dd'][1]=='1')
				update_user_meta( $user_id, 'user_tg_dd', $_POST['user_tg_dd']);
		}


		if ( !empty( $_POST['user_khong_dd'] ) && !empty( $_POST['user_tgvn_dd'] ))
		{
			if($_POST['user_tgvn_dd'][1]=='1')
				update_user_meta( $user_id, 'user_tgvn_dd', $_POST['user_tgvn_dd'] );
		}
	}
}
add_action('user_profile_update_errors', 'nh_save_extra_profile_fields', 10, 3);



/*---------ADD EXTRA FIELDS TO ADMIN PROFILE-------------*/
//add_action( 'show_user_profile', 'nh_show_extra_profile_fields' );
//add_action( 'edit_user_profile', 'nh_show_extra_profile_fields' );
add_action( 'the_action_hook', 'nh_show_extra_profile_fields' );

function nh_show_extra_profile_fields( $user ) {
$user_id = $user->ID;

$user_idtv_dd = get_user_meta($user_id,'user_idtv_dd',true);

$user_address_dd = get_user_meta($user_id,'user_address_dd',true);	//true ra value / flase Mang?
$user_bday = get_user_meta($user_id,'user_bday',true);
$user_phone_dd = get_user_meta($user_id,'user_phone_dd',true);
$user_sex = get_user_meta($user_id,'user_sex',true);	//true ra value / flase Mang?
$user_ht = get_user_meta($user_id,'user_ht',true);
$user_ocupation_dd = get_user_meta($user_id,'user_ocupation_dd',true);
$user_level_dd = get_user_meta($user_id,'user_level_dd',true);

$user_bc_dd = get_user_meta($user_id,'user_bc_dd',true);
$user_address_tr_dd = get_user_meta($user_id,'user_address_tr_dd',true);
$user_school_dd = get_user_meta($user_id,'user_school_dd',true);
$user_slcv_dd = get_user_meta($user_id,'user_slcv_dd',true);

$user_ld_dd = get_user_meta($user_id,'user_ld_dd',true);
$user_vlnb_dd = get_user_meta($user_id,'user_vlnb_dd',true);
$user_htn_dd = get_user_meta($user_id,'user_htn_dd',true);

$user_di_dd = get_user_meta($user_id,'user_di_dd',true);
$user_khong_dd = get_user_meta($user_id,'user_khong_dd',true);

$user_tg_dd = get_user_meta($user_id,'user_tg_dd',true);
$user_tgvn_dd = get_user_meta($user_id,'user_tgvn_dd',true);




	 ?>

		<tr>
			<td><label for="user_idtv_dd"><?php _e('Mã thành viên','theme-my-login'); ?></label></td>
			<td><input type="text" name=""  id="" class="input" value="<?php echo esc_attr( $user_idtv_dd ); ?>" size="20" disabled="disabled"/></td>
		</tr>

		<tr>
			<td><label for="user_ht"><?php _e('Họ và tên','theme-my-login'); ?> <span class="description"><?php _e( '(*)' ); ?></span></label></td>
			<td><input type="text" name="user_ht"  id="user_ht" class="input" value="<?php echo esc_attr( $user_ht ); ?>" size="20" /></td>
		</tr>
		<tr>
			<td><label for="user_bday"><?php _e( 'Ngày sinh','theme-my-login' );?></span></label></td>
			<td><input type="date" name="user_bday" max="2050-12-31" id="user_bday" value="<?php echo esc_attr( $user_bday ); ?>" class="regular-text" /></td>
		</tr>

		<tr>
			<td><label for="user_sex"><?php _e('Giới tính','theme-my-login'); ?></label></td>
			<td>
				<input type="radio" name="user_sex" value="1" <?php if(esc_attr( $user_sex )=="1") echo 'checked="checked"'; ?>><?php _e('Nam', 'theme-my-login' ); ?>&nbsp;&nbsp;
				<input type="radio" name="user_sex" value="2" <?php if(esc_attr( $user_sex )=="2") echo 'checked="checked"'; ?>><?php _e('Nữ', 'theme-my-login' ); ?>
			</td>
		</tr>
		<tr>
			<td><label for="user_phone_dd"><?php _e( 'Điện thoại liên hệ','theme-my-login' );?><span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><input type="tel"  name="user_phone_dd" id="txtPhone" value="<?php echo esc_attr( $user_phone_dd ); ?>" class="regular-text" /><span id="spnPhoneStatus"></span></td>
		</tr>
		<tr>
			<td><label for="user_address_tr_dd"><?php _e( 'Hộ khẩu thường trú','theme-my-login' );?><span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><input type="text" name="user_address_tr_dd"  id="user_address_tr_dd" value="<?php echo esc_attr( $user_address_tr_dd ); ?>" class="regular-text" /></td>
		</tr>

		<tr>
			<td><label for="user_address_dd"><?php _e( 'Nơi ở hiện nay','theme-my-login' );?><span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><input type="text" name="user_address_dd"  id="user_address_dd" value="<?php echo esc_attr( $user_address_dd ); ?>" class="regular-text" /></td>
		</tr>

		<tr>
			<td><label for="user_level_dd"><?php _e('Trình độ tiếng Nhật','theme-my-login'); ?></label></td>

			<td><select name="user_level_dd" id="user_level_dd">


				  <option value="1" <?php selected( esc_attr( $user_level_dd ), 1 ); ?>>N1</option>
				  <option value="2" <?php selected( esc_attr( $user_level_dd ), 2 ); ?>>N2</option>
				  <option value="3" <?php selected( esc_attr( $user_level_dd ), 3 ); ?>>N3</option>
				  <option value="4" <?php selected( esc_attr( $user_level_dd ), 4 ); ?>>N4</option>
				  <option value="5" <?php selected( esc_attr( $user_level_dd ), 5 ); ?>>N5</option>
				  <option value="6" <?php selected( esc_attr( $user_level_dd ), 6 ); ?>>Chưa biết tiếng Nhật</option>

				</select></td>
		</tr>

		<tr>
			<td><label for="user_bc_dd"><?php _e('Bằng cấp (nêu rõ chuyên ngành)','theme-my-login'); ?></span></label></td>
			<td><input type="text" name="user_bc_dd" id="user_bc_dd" class="input" value="<?php echo esc_attr( $user_bc_dd ); ?>" size="20" /></td>

		</tr>

		<tr>
			<td><label for="user_school_dd"><?php _e('Nếu là sinh viên nêu rõ ngành học, trường','theme-my-login'); ?></span></label></td>
			<td><input type="text" name="user_school_dd" id="user_school_dd" class="input" value="<?php echo esc_attr( $user_school_dd ); ?>" size="20" /></td>

		</tr>

		<tr>
			<td><label for="user_ocupation_dd"><?php _e('Công việc hiện tại (nếu có)','theme-my-login'); ?></label></td>
			<td><input type="text" name="user_ocupation_dd" id="user_ocupation_dd" class="input" value="<?php echo esc_attr( $user_ocupation_dd ); ?>" size="20" /></td>

		</tr>

		<tr>
			<td><label for="user_slcv_dd"><?php _e('Nêu sơ lược về kinh nghiệm công việc đã từng làm','theme-my-login'); ?> <span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><textarea name="user_slcv_dd" id="user_slcv_dd" rows="5"  class="description" cols="30"><?php echo esc_attr( $user_slcv_dd ); ?></textarea></td>

		</tr>

		<tr>
			<td><label for="user_tl_dd"><?php _e('Bạn đã từng','theme-my-login'); ?></span></label></td>
			<td>
					<input type="hidden" name="user_di_dd[]" id="user_di_dd"  value="0"  >
					<input type="checkbox" name="user_di_dd[]" id="user_di_dd"  value="1" <?php if(esc_attr($user_di_dd)=='1') echo 'checked'; ?> >&nbsp;&nbsp;Đi Nhật


					<br>Khoảng thời gian:&nbsp;&nbsp;<input type="text" name="user_tg_dd" id="user_tg_dd" class="input" value="<?php echo esc_attr( $user_tg_dd ); ?>" size="20" />
					<br>
					<input type="hidden" name="user_khong_dd[]" id="user_khong_dd"  value="0"  >
					<input type="checkbox" name="user_khong_dd[]"  value="1" id="user_khong_dd"  <?php if(esc_attr( $user_khong_dd )=='1') echo 'checked'; ?> >&nbsp;&nbsp;Làm việc cho công ty Nhật tại Việt Nam

					<br> Khoảng thời gian:&nbsp;&nbsp;<input type="text" name="user_tgvn_dd" id="user_tgvn_dd" class="input" value="<?php echo esc_attr( $user_tgvn_dd ); ?>" size="20" />
					<br>
			</td>

		</tr>
		<tr>
			<td><label for="user_ld_dd"><?php _e('Lý do bạn muốn làm việc tại Nhật Bản','theme-my-login'); ?> <span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><textarea name="user_ld_dd" id="user_ld_dd"  class="description" rows="5" cols="30"><?php echo esc_attr( $user_ld_dd ); ?></textarea></td>

		</tr>

		<tr>
			<td><label for="user_vlnb_dd"><?php _e('Bạn muốn làm việc gì tại Nhật Bản','theme-my-login'); ?> <span class="description"><?php _e( ' (*)' ); ?></span></label></td>
			<td><input type="text" name="user_vlnb_dd" id="user_vlnb_dd" class="input" value="<?php echo esc_attr( $user_vlnb_dd ); ?>" size="20" /></td>

		</tr>
		<tr>
			<td><label for="user_htn_dd"><?php _e('Bạn có muốn học tiếng Nhật tại Câu lạc bộ?','theme-my-login'); ?></label></td>

			<td><input type="radio" name="user_htn_dd" value="1" <?php if(esc_attr( $user_htn_dd )==1) echo 'checked="checked"'; else echo 'checked="checked"';?> ><?php _e( 'Có', 'theme-my-login' ); ?>&nbsp;&nbsp;<input type="radio" name="user_htn_dd" value="2" <?php if(esc_attr( $user_htn_dd )==2) echo 'checked="checked"'; ?>><?php _e('Không', 'theme-my-login' ); ?></td>



		</tr>







 <?php
 }

// STOP HERE
?>