<?php
	/**
	 * The header widget area is triggered if any of the areas
	 * have widgets. So let's check that first.
	 *
	 * If none of the sidebars have widgets, then let's bail early.
	 */
	if (   ! is_active_sidebar( 'header-search' ) )
		return;
	// If we get this far, we have widgets. Let do this.
?>

	<?php if ( is_active_sidebar( 'header-search' ) ) : ?>
			<?php dynamic_sidebar( 'header-search' ); ?>
	<?php endif; ?>
