<?php
/**
 * itek functions and definitions
 *
 * @package iTek
 */

if ( ! function_exists( 'itek_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function itek_setup() {
	
	/**
     * Set the content width based on the theme's design and stylesheet.
     */
    global $content_width;
	if ( ! isset( $content_width ) ) {
	   $content_width = 780; /* pixels */
	}
	
	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on itek, use a find and replace
	 * to change 'itek' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'itek', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'itek-full-width', 1380, 770, true );
	add_image_size( 'itek-page-feature', 1230, 500, true ); //(cropped)
	add_image_size( 'itek-post-feature', 280, 180, true ); //(cropped)
	add_image_size( 'itek-post-standard', 940, 500, true ); //(cropped)
	add_image_size( 'itek-recentpost-thumb', 372, 176, true ); //(cropped)

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary'   => __( 'Primary Menu', 'itek' ),
		'secondary' => __( 'Secondary Menu', 'itek' ),
		'social'    => __( 'Social Menu', 'itek' ),
		'footer'    => __( 'Footer Menu', 'itek' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
	
	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'itek_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );
}
endif; // itek_setup
add_action( 'after_setup_theme', 'itek_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function itek_widgets_init() {
	require get_template_directory() . '/inc/widgets.php';
	register_widget( 'iTek_Ephemera_Widget' );
	register_widget( 'iTek_RecentPostWidget' );
	register_widget( 'iTek_RelatedProductWidget' );
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'itek' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Showcase One - Above Content.', 'itek' ),
		'id' => 'header-showcase1',
		'description' => __( 'One of four above content showcase widget - ideal for use with the iTek - Alternative Recent Posts widget', 'itek' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Showcase Two - Above Content.', 'itek' ),
		'id' => 'header-showcase2',
		'description' => __( 'One of four above content showcase widget - ideal for use with the iTek - Alternative Recent Posts widget', 'itek' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Showcase Three - Above Content', 'itek' ),
		'id' => 'header-showcase3',
		'description' => __( 'One of four above content showcase widget - ideal for use with the iTek - Alternative Recent Posts widget', 'itek' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'itek_widgets_init' );

if ( !class_exists( 'PageShowcaseWidget' ) ) :
    include(get_template_directory() . '/inc/itek-page-showcase.php');
endif;

/**
 * Returns number of widgets in a widget position - used in the Header Showcase widget area.
 *
 * @since Itek 1.0.0
 */
function itek_header_sidebar_class() {
	$count = 0;

	if ( is_active_sidebar( 'header-showcase1' ) )
		$count++;

	if ( is_active_sidebar( 'header-showcase2' ) )
		$count++;

	if ( is_active_sidebar( 'header-showcase3' ) )
		$count++;

	$class = '';

	switch ( $count ) {
		case '1':
			$class = 'one';
			break;
		case '2':
			$class = 'two';
			break;
		case '3':
			$class = 'three';
			break;
	}

	if ( $class )
		echo 'class="' . $class . '"';
}

// Itek Custom Excerpt
function itek_trim_excerpt($itek_excerpt) {
  $raw_excerpt = $itek_excerpt;
  if ( '' == $itek_excerpt ) {
    $itek_excerpt = get_the_content(''); // Original Content
    $itek_excerpt = strip_shortcodes($itek_excerpt); // Minus Shortcodes
    $itek_excerpt = apply_filters('the_content', $itek_excerpt); // Filters
    $itek_excerpt = str_replace(']]>', ']]&gt;', $itek_excerpt); // Replace
    
	if (get_theme_mod( 'itek_feed_excerpt_length' )) :
		$feedlimit = get_theme_mod( 'itek_feed_excerpt_length' );
	else :
		$feedlimit = '85';
	endif;
    $excerpt_length = apply_filters('excerpt_length', $feedlimit); // Length
    $itek_excerpt = wp_trim_words( $itek_excerpt, $excerpt_length );
    
    // Use First Video as Excerpt
    $postcustom = get_post_custom_keys();
    if ($postcustom){
      $i = 1;
      foreach ($postcustom as $key){
        if (strpos($key,'oembed')){
          foreach (get_post_custom_values($key) as $video){
            if ($i == 1){
              $itek_excerpt = $video.$itek_excerpt;
            }
          $i++;
          }
        }  
      }
    }
  }
  return apply_filters('itek_trim_excerpt', $itek_excerpt, $raw_excerpt);
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'itek_trim_excerpt');

// Lets do a separate excerpt length for the alternative recent post widget
function itek_recentpost_excerpt () {
	$theContent = trim(strip_tags(get_the_content()));
		$output = str_replace( '"', '', $theContent);
		$output = str_replace( '\r\n', ' ', $output);
		$output = str_replace( '\n', ' ', $output);
			if (get_theme_mod( 'itek_recentpost_excerpt_length' )) :
			$limit = get_theme_mod( 'itek_recentpost_excerpt_length' );
			else : 
			$limit = '14';
			endif;
			$content = explode(' ', $output, $limit);
			array_pop($content);
		$content = implode(" ",$content)."  ";
	return strip_tags($content, ' ');
}

/**
 * Register Lato Google font for iTek.
 *
 * @since iTek 1.0
 *
 * @return string
 */
function itek_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'PT+Sans|Lato font: on or off', 'itek' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'PT+Sans|Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}


/**
 * Enqueue scripts and styles for the front end.
 *
 * @since iTek 1.0
 *
 * @return void
 */
 
function itek_ie_support_header() {
    echo '<!--[if lt IE 9]>'. "\n";
    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/html5shiv.js' ) . '"></script>'. "\n";
    echo '<![endif]-->'. "\n";
}
add_action( 'wp_head', 'itek_ie_support_header', 1 );

/**
 * Enqueue scripts and styles
 */
function itek_scripts() {
	global $wp_styles;
	// Bump this when changes are made to bust cache
    $itek_theme = wp_get_theme();
    $version = $itek_theme->get( 'Version' );
	// CSS Scripts
    // Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'itek-lato', itek_font_url(), array(), null );
		
	wp_enqueue_style('itek-style', get_template_directory_uri().'/css/style.css', false ,$version, 'all' );
	wp_enqueue_style('itek-bootstrap', get_template_directory_uri().'/css/app.css', false ,$version, 'all' );
    wp_enqueue_style('itek-responsive', get_template_directory_uri().'/css/app-responsive.css', false ,$version, 'all' );
	wp_enqueue_style('itek-custom', get_template_directory_uri().'/css/custom.css', false ,$version, 'all' );
		
	// Load style.css from child theme
    if (is_child_theme()) {
        wp_enqueue_style('itek_child', get_stylesheet_uri(), false, $version, null);
    }
	
	// Loads the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'itek-ie', get_template_directory_uri() . '/css/ie.css', array( 'itek-style' ), $version );
	wp_style_add_data( 'itek-ie', 'conditional', 'lt IE 8' );
		
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    	
	wp_enqueue_script('bootstrap.min.js', get_template_directory_uri().'/js/app.js', array('jquery'),$version, true );
	
	wp_enqueue_script( 'itek-bootstrapnav', get_template_directory_uri() . '/js/twitter-bootstrap-hover-dropdown.js', array(), $version, true );
	
	wp_enqueue_script( 'itek-custom-js', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), false, true );
    
	wp_enqueue_script('modernizr.js', get_template_directory_uri().'/js/modernizr.custom.79639.js', array('jquery'),$version, false );
	
	$itek_header_image_link = get_header_image();
	wp_localize_script( 'itek-custom-js', 'itekScriptParam', array('itek_image_link'=> $itek_header_image_link ) );
		
}
add_action( 'wp_enqueue_scripts', 'itek_scripts' );

if ( get_theme_mod( 'itek_fitvids_enable' ) != 0 ) {

function itek_fitvids_scripts() {
$itek_theme = wp_get_theme();
$version = $itek_theme->get( 'Version' );
wp_enqueue_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), $version, true );
} // end fitvids_scripts
add_action('wp_enqueue_scripts','itek_fitvids_scripts', 20);
// selector script
function itek_fitthem() { ?>
   	<script type="text/javascript">
   	jQuery(document).ready(function() {
   		jQuery('<?php echo get_theme_mod('itek_fitvids_selector'); ?>').fitVids({ customSelector: '<?php echo stripslashes(get_theme_mod('itek_fitvids_custom_selector')); ?>'});
   	});
   	</script>
<?php } // End selector script
add_action( 'wp_footer', 'itek_fitthem', 210 );
}


/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since iTek 1.0
 *
 * @return void
 */
function itek_admin_fonts() {
	wp_enqueue_style( 'itek-lato', itek_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'itek_admin_fonts' );

require get_template_directory() . '/inc/nav-menu-walker.php';

require get_template_directory() . '/inc/custom-header.php';

if ( ! function_exists( 'itek_internal_css' ) ) :
// Implement Custom Header features.

/**
 * Hooks the Custom Internal CSS to head section
 */
function itek_internal_css() {
	if ( get_header_image() ) : 
		$header_image_height = get_custom_header()->height;
		if ( get_theme_mod( 'itek_tagline_visibility' ) != 0 ) {
		    $height = $header_image_height -25;
        } else {
            $height = $header_image_height +20;
        }	

		$header = get_header_image();

		$header_image = "background-image: url('$header');";
		$header_repeat = " background-repeat: repeat-x;";
		$header_position = " background-position: center top;";
		$header_attachment = " background-attachment: scroll;";
		$header_image_style = $header_image . $header_repeat . $header_position . $header_attachment;
		if ( get_theme_mod( 'itek_headerimage_visibility' ) != 0 ) { ?>
		<style type="text/css" id="custom-header-css">
		#parallax-bg { <?php echo trim( $header_image_style ); ?> } .site { margin-top: <?php echo $height/2; ?>px; } 
		</style>
		<?php } elseif ( is_front_page() ) { ?>
		<style type="text/css" id="custom-header-css">
		#parallax-bg { <?php echo trim( $header_image_style ); ?> } .site { margin-top: <?php echo $height/2; ?>px; } 
		</style>
		<?php }
	endif;
}
add_action('wp_head', 'itek_internal_css');
endif;

if ( class_exists( 'woocommerce' ) ) {
/**
 * Custom WooCommerce functions for this theme.
 */
require get_template_directory() . '/inc/woo-functions.php';
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since iTek 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function itek_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}
	return $classes;
}
add_filter( 'post_class', 'itek_post_classes' );

/*
 * wc_remove_related_products
 * 
 * Clear the query arguments for related products so none show.
 * Add this code to your theme functions.php file.  
 */
function wc_remove_related_products( $args ) {
	return array();
}
//add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 

/*
 * Hide Price and Add to Cart
 * 
 * Hide prices and checkout functionality in woocommerce.  
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

/*
 * Add shortcode to widget
 */
add_filter('widget_text', 'do_shortcode');

/*
 * shortcode to get post
 */
function create_shortcode_post() {
 
        $random_query = new WP_Query(array(
                'post_type'      => 'post',
				'posts_per_page' => 1,
				'category_name'            => 'cam-nang-du-hoc'
        ));
 
        ob_start();
        if ( $random_query->have_posts() ) :
                
                while ( $random_query->have_posts() ) :
                        $random_query->the_post();?>
 
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'itek-post-feature' );?><h3><?php the_title(); ?></h3></a>
								<?php the_excerpt();?>
 
                <?php endwhile;
                
        endif;
        $list_post = ob_get_contents(); //Lấy toàn bộ nội dung phía trên bỏ vào biến $list_post để return
 
        ob_end_clean();
 
        return $list_post;
}
add_shortcode('cam-nang-du-hoc', 'create_shortcode_post');

/*
 * shortcode contact info
 */
function create_shortcode_contact_info() {
	?>
	<div class="contact-info">
		<p>Hãy gọi ngay để chúng tôi có thể tư vấn trực tiếp cho bạn</p>
		<p class="hot-line">Hot-line: 08-7309-1212<p>
		<p class="hot-line">Email: lbvan@aline.jp<p>		
	</div>
	<div class="contact-slogan">
		<p><span class="thumbs-up"><i class="fa fa-star-o"></i></span><span class="thumbs-content"> Uy tín - Chất lượng đặt lên hàng đầu</span></p>
	</div>
	<div class="contact-slogan">
		<p><span class="thumbs-up"><i class="fa fa-star-o"></i></span><span class="thumbs-content"> Thành công của bạn là mục tiêu của chúng tôi</span></p>
	</div>
	<div class="contact-slogan">
		<p><span class="thumbs-up"><i class="fa fa-star-o"></i></span><span class="thumbs-content"> ĐÔNG DU là người bạn đồng hành đáng tin cậy</span></p>
	</div>
	<?php
        
}
add_shortcode('contact_info', 'create_shortcode_contact_info');

/*
 * shortcode step
 */
function create_shortcode_step_info() {
	?>
	<div class="metro row">
		<div class="span4 no-tablet-portrait no-phone" style="margin-right:3%;">
			<a href="<?php echo site_url();?>/gioi-thieu/"><div class="notice marker-on-right bg-lighterBlue padding20 text-center">
				<h1 class="fg-white" style="font-size: 40px; line-height: 55px; margin-bottom: 30px;  margin-top: 30px;">Thế mạnh<br />của<br />Đông Du</h1>
			</div></a>
		</div>
		<div class="span8 sevenstep" style="margin-right:0;  margin-top: 15px;">
			<ol class="styled">
				<li style="width: 90%">Có văn phòng hỗ trợ du học sinh tại Nhật Bản (có nhân viên Việt Nam)</li>
				<li style="width: 90%">Đảm bảo 100% du học sinh sẽ có việc làm khi du học thông qua công ty (làm các công việc ở công ty mẹ)</li>
				<li style="width: 90%">Là công ty duy nhất tại Việt Nam có website tập hợp nhiều thông tin của các trường Đại Học, Cao Đẳng, trường tiếng Nhật,…website còn có chức năng so sánh các trường, giúp du học sinh dễ dàng định hướng và lựa chọn hơn.</li>
				<li style="width: 90%">Chi phí thấp, đội ngũ nhân viên có kinh nghiệm về du học sẽ hỗ trợ nhiệt tình với tư cách những người đi trước để truyền đạt lại kinh nghiệm.</li>				
			</ol>
		</div>
	</div>
	<?php
        
}
add_shortcode('step_info', 'create_shortcode_step_info');

/*
 * shortcode Topschools
 */
 function get_product_category_by_id( $category_id ) {
    $term = get_term_by( 'id', $category_id, 'product_cat', 'ARRAY_A' );
    return $term['name'];
}
 
function create_shortcode_topschools() {
?>	
	<!--slider-->
	<script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery-1.7.2.min.js'></script>
	<script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.easing.1.3.js?ver=1.3'></script>
	<script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/camera.min.js?ver=1.3.4'></script>
	<!--slider-->
<div class="content-box">
<div class="content-padding">
<h1 class="widget-title">Các trường hàng đầu Nhật bản</h1>
</div>
</div>	
<div class="slider-wrap">
<div id="slider-wrapper" class="slider">
<script type="text/javascript">
jQuery.noConflict();
    //jQuery(window).load(function() {
		jQuery(function() {
			var myCamera = jQuery('#camera555c1239885a0');
			if (!myCamera.hasClass('motopress-camera')) {
				myCamera.addClass('motopress-camera');
				myCamera.camera({
					alignment           : 'topCenter', //topLeft, topCenter, topRight, centerLeft, center, centerRight, bottomLeft, bottomCenter, bottomRight
					autoAdvance         : true,   //true, false
					mobileAutoAdvance   : true, //true, false. Auto-advancing for mobile devices
					barDirection        : 'leftToRight',    //'leftToRight', 'rightToLeft', 'topToBottom', 'bottomToTop'
					barPosition         : 'top',    //'bottom', 'left', 'top', 'right'
					cols                : 12,
					easing              : 'easeOutQuad',  //for the complete list http://jqueryui.com/demos/effect/easing.html
					mobileEasing        : '',   //leave empty if you want to display the same easing on mobile devices and on desktop etc.
					fx                  : 'curtainTopLeft',    //'random','simpleFade', 'curtainTopLeft', 'curtainTopRight', 'curtainBottomLeft',          'curtainBottomRight', 'curtainSliceLeft', 'curtainSliceRight', 'blindCurtainTopLeft', 'blindCurtainTopRight', 'blindCurtainBottomLeft', 'blindCurtainBottomRight', 'blindCurtainSliceBottom', 'blindCurtainSliceTop', 'stampede', 'mosaic', 'mosaicReverse', 'mosaicRandom', 'mosaicSpiral', 'mosaicSpiralReverse', 'topLeftBottomRight', 'bottomRightTopLeft', 'bottomLeftTopRight', 'bottomLeftTopRight'
													//you can also use more than one effect, just separate them with commas: 'simpleFade, scrollRight, scrollBottom'
					mobileFx            : '',   //leave empty if you want to display the same effect on mobile devices and on desktop etc.
					gridDifference      : 250,  //to make the grid blocks slower than the slices, this value must be smaller than transPeriod
					height              : '52.5%', //here you can type pixels (for instance '300px'), a percentage (relative to the width of the slideshow, for instance '50%') or 'auto'
					imagePath           : 'images/',    //he path to the image folder (it serves for the blank.gif, when you want to display videos)
					loader              : 'no',    //pie, bar, none (even if you choose "pie", old browsers like IE8- can't display it... they will display always a loading bar)
					loaderColor         : '#ffffff',
					loaderBgColor       : '#eb8a7c',
					loaderOpacity       : 1,    //0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1
					loaderPadding       : 0,    //how many empty pixels you want to display between the loader and its background
					loaderStroke        : 3,    //the thickness both of the pie loader and of the bar loader. Remember: for the pie, the loader thickness must be less than a half of the pie diameter
					minHeight           : '147px',  //you can also leave it blank
					navigation          : false, //true or false, to display or not the navigation buttons
					navigationHover     : false,    //if true the navigation button (prev, next and play/stop buttons) will be visible on hover state only, if false they will be visible always
					pagination          : true,
					playPause           : false,   //true or false, to display or not the play/pause buttons
					pieDiameter         : 33,
					piePosition         : 'rightTop',   //'rightTop', 'leftTop', 'leftBottom', 'rightBottom'
					portrait            : true, //true, false. Select true if you don't want that your images are cropped
					rows                : 8,
					slicedCols          : 12,
					slicedRows          : 8,
					thumbnails          : false,
					time                : 7000,   //milliseconds between the end of the sliding effect and the start of the next one
					transPeriod         : 1200, //lenght of the sliding effect in milliseconds

									////////callbacks

					onEndTransition     : function() {  },  //this callback is invoked when the transition effect ends
					onLoaded            : function() {  },  //this callback is invoked when the image on a slide has completely loaded
					onStartLoading      : function() {  },  //this callback is invoked when the image on a slide start loading
					onStartTransition   : function() {  }   //this callback is invoked when the transition effect starts
				});
			}
		});
//    });
</script>
<div id="camera555c1239885a0" class="camera_wrap camera">
<?php
     $args = array( 'post_type' => 'product', 'meta_key' => '_featured','posts_per_page' => 7, 'meta_value' => 'yes' );
     $loop = new WP_Query( $args );
     while ( $loop->have_posts() ) : $loop->the_post(); global $product;
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'shop_single_image_size', true);
		$terms = get_the_terms( $loop->post->ID, 'product_cat' );
		foreach ($terms as $term) {
			$product_cat_id = $term->term_id;
			break;
		}
?>                      
		<div data-src='<?php echo $thumb_url[0]; ?>' data-link='<?php echo get_permalink( $loop->post->ID ) ?>' data-thumb='<?php echo $thumb_url[0]; ?>'>
		<div class="camera_caption fadeFromTop">
		<h4 style="line-height: 15px;"><i class="fa fa-map-marker"></i> <?php echo get_product_category_by_id( $product_cat_id ) ?></h4>
		<h1 class="widget-title"  style="line-height: 25px;"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><?php the_title(); ?></a></h1>

		<!--<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-primary btn-large">Read More</a>-->
		</div>
		</div>
	 
<?php endwhile; ?>
<?php wp_reset_query(); ?>
</div> 
</div> 
</div>
	<?php
        
}
add_shortcode('topschools', 'create_shortcode_topschools');

/*remove product meta: category...*/
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/*Marking future post as published*/
function future_posts_now() {
global $wpdb;
$tposts = $wpdb->posts;
$sql = "UPDATE	$tposts
		SET		post_status = 'publish'
		WHERE	post_status = 'future'
		AND		post_type = 'product'";
$wpdb->query($sql);
}
add_action('init', 'future_posts_now');

/*Remove Additional Information Tab*/
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
 
    unset( $tabs['additional_information'] );   // Remove the additional information tab
 
    return $tabs;
}

/**
 * @desc Remove quantity in all product type
 */
function wc_remove_all_quantity_fields( $return, $product ) {
    return true;
}
add_filter( 'woocommerce_is_sold_individually', 'wc_remove_all_quantity_fields', 10, 2 );

/**
 * Change the add to cart text on single product pages
 */
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');

function woo_custom_cart_button_text() {

  global $woocommerce;
	
	foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Already in cart - Add Again?', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}

/**
 * Change the add to cart text on product archives
 */
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );

function woo_archive_custom_cart_button_text() {

	global $woocommerce;
	
	foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Already in cart', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}

//Display WooCommerce Product Variation As Table Instead of Drop down
function woocommerce_variable() {
		global $product, $post;
		
		$variations = $product->get_available_variations();
		echo "<p><table>";
		
		foreach ($variations as $key => $value) {
			$product_variation = new WC_Product_Variation($value['variation_id']);
			$attr_order[] = 'attribute_'.$key;
			$attr_order = array();
			$orderedattributes = array_merge(array_flip($attr_order), $value['attributes']);
            $attrnames = array();
            foreach ($orderedattributes as $taxon => $taxval) {
			
              $temp = '';
              $temp = get_term_by('slug', $taxval, str_replace('attribute_', '', $taxon));
              if ($temp !== false) {
                $attrnames[$taxval] = $temp->name;
              } else {
              
                // get all custom attributes sanitize_title
                $allcustomattr = explode('|',$product->get_attribute( str_replace('attribute_', '', $taxon) ));
                $customattrnames = array();
                foreach ($allcustomattr as $customattrname) {
                  $customattrnames[sanitize_title(trim($customattrname))] = $customattrname;
                }
                if ($taxval) {
                  $attrnames[$taxval] = $customattrnames[$taxval];
                }
              }
            }
          
			echo '<tr><td class="optionscol">
              '. implode( apply_filters( 'vartable_attributes_join', '</td><td>'), apply_filters( 'vartable_attributes_array', $attrnames)) .'
            </td>
            ';
			echo "<td>".$product_variation->get_price_html()."</td></tr>";
			//echo "<td>".$value['price_html']."</td></tr>";			
         } 
		 if (count($attrnames)==4): ?>
		<thead>
			<tr><th>Khóa học</th><th>Tháng nhập học</th><th>Thời gian học</th><th>Số giờ học</th><th>Học phí</th></tr>
		</thead><?php else : ?>
		<thead>
			<tr><th>Khóa học</th><th>Thời gian học</th><th>Số giờ học</th><th>Học phí</th></tr>
		</thead>
		<?php endif;
		 echo "</table></p>";  
}
add_shortcode('variable', 'woocommerce_variable');

//Function tu tim link trong bai viet
function autolink($str, $attributes=array()) {
	$attrs = '';
	foreach ($attributes as $attribute => $value) {
		$attrs .= " {$attribute}=\"{$value}\"";
	}

	$str = ' ' . $str;
	$str = preg_replace(
			'`([^"=\'>])((http|https|ftp)://[^\s<]+[^\s<\.)])`i',
			'$1<a href="$2"'.$attrs.'>$2</a>',
			$str
	);
	//$email_pattern =  '[:en](..email invisible..)[:vi](..email đã được ẩn..)[:ja](..email invisible..)';
	//$str = preg_replace('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i',__($email_pattern),$str); // extract email
	//$str = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $str);
	$str = substr($str, 1);
	 
	return $str;
}

function woo_the_content( $more_link_text = null, $strip_teaser = false) {
	$content = get_the_content( $more_link_text, $strip_teaser );

	/**
	 * Filter the post content.
	 *
	 * @since 0.71
	 *
	 * @param string $content Content of the current post.
	 */
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	$content = autolink( $content, array("target"=>"_blank","rel"=>"nofollow"));
	echo $content;
}

/**
 * Gets the url to the cart page.
 *
 * @return string url to page
 */
function itek_get_cart_url() {
	$cart_page_id = wc_get_page_id( 'dang-ky-tu-van' );
	return apply_filters( 'woocommerce_get_cart_url', $cart_page_id ? get_permalink( $cart_page_id ) : '' );
}

/*Add .html to Woocommerce permalinks*/
function wpse_178112_permastruct_html( $post_type, $args ) {
    if ( $post_type === 'product' )
        add_permastruct( $post_type, "{$args->rewrite['slug']}/%$post_type%.html", $args->rewrite );
}

add_action( 'registered_post_type', 'wpse_178112_permastruct_html', 10, 2 );
/*For categories*/
function wpse_178112_category_permastruct_html( $taxonomy, $object_type, $args ) {
    if ( $taxonomy === 'product_cat' ) 
        add_permastruct( $taxonomy, "{$args['rewrite']['slug']}/%$taxonomy%.html", $args['rewrite'] );
}

add_action( 'registered_taxonomy', 'wpse_178112_category_permastruct_html', 10, 3 );