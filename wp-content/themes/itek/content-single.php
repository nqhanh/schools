<?php
/**
 * @package iTek
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header single">
		
		<h1 class="entry-title"><?php the_title(); ?></h1>
			<!--<div class="entry-thumbnail">
			   <?php the_post_thumbnail( 'itek-post-standard' ); ?>
			</div>-->
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			/*wp_link_pages( array(
				'before' => '<div class="page-links btn special">' . __( 'Pages: ', 'itek' ),
				'after'  => '</div>',
			) );*/
		?>		
	</div><!-- .entry-content -->

	<footer class="entry-meta">
	
			<?php itek_posted_on(); ?>
		
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'itek' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'itek' ) );

			if ( ! itek_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'itek' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'itek' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'itek' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'itek' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit', 'itek' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
	
	<div class="share-social">
							<div class="fb-share">
								<div class="fb-share-button" data-href="<?php echo get_permalink();?>" data-type="button"></div>
							</div>
							<div class="fb-buttom-like">
							<div class="fb-like" data-href="<?php the_permalink();?>" data-colorscheme="light" data-layout="button_count" data-action="like"
								data-show-faces="false" data-send="false"></div>
							</div>
							<div class="google-plus">
								<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
								<g:plusone size="medium" data-height="20"></g:plusone>
							</div>
							<div class="share-twitter">
								<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php the_title();?>">Tweet</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https'; 
									if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';
									fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							</div>
				</div>
				<!--<div class="fb-comments" data-href="<?php echo get_permalink();?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>-->
	

<h3 id="relate-single"><?php _e('Bài cùng chuyên mục:', 'responsive');?></h3>
<div id="relate-post-single">
	<?php

	$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 7, 'post__not_in' => array($post->ID) ) );
	if( $related ) foreach( $related as $post ) {
	setup_postdata($post); ?>
	 <ul style="margin: 0 1.5em 0.4em 0;padding-left: 2.0em;"> 
			<li style="list-style: square inside;display: list-item;">
			<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
		</ul>   
	<?php }
	wp_reset_postdata(); ?>
</div>
</article><!-- #post-## -->