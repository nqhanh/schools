
</div>
</div><!-- #page -->
  
<?php do_action( 'itek_before_footer_nav' ); ?>
    <?php if ( has_nav_menu( 'footer' ) ) : // Check if there's a menu assigned to the 'social' location. ?>
        <?php get_template_part( 'nav-footer' ); 
    endif; // End check for menu. ?>
<?php do_action( 'itek_after_footer_nav' ); ?>
<?php if ( ! wp_is_mobile()) {?>
<div class="rainbow-preloader">
  <div class="rainbow-stripe"></div>
  <div class="rainbow-stripe"></div>
  <div class="rainbow-stripe"></div>
  <div class="rainbow-stripe"></div>
  <div class="rainbow-stripe"></div>
  <div class="shadow"></div>
  <div class="shadow"></div>
</div>
<?php }?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<?php do_action( 'itek_in_before_colophon' ); ?>
		<div class="site-info">
		<div class="container">
			<?php do_action( 'itek_before_credits' ); ?>
			<p>Copyright © A-LINE ltd. and A-LINE Vietnam Co., Ltd.</p>
			<p>8F, Room 801, Zen Plaza, 54-56 Nguyen Trai, Ben Thanh Ward, District 1, HCM City, Vietnam.</p>
			<p>Hot-line: 08-7309-1212 - Email: lbvan@aline.jp</p>
		<?php do_action( 'itek_after_credits' ); ?>
	    </div>
	    </div><!-- .site-info -->
<?php do_action( 'itek_in_after_colophon' ); ?>
</footer><!-- #colophon -->
<?php do_action( 'itek_below_footer' ); ?>
	
	<?php wp_footer(); ?>
	</body>
</html>