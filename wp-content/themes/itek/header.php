<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 		
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">	
    <?php wp_head(); ?>
	
	<!-- Datetime picker -- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/bootstrap331.min.js"></script>
	<script src="<?php echo bloginfo('template_directory');?>/js/moment-with-locales.js"></script>
	<script src="<?php echo bloginfo('template_directory');?>/js/bootstrap-datetimepicker.js"></script>
	<link href="<?php echo bloginfo('template_directory');?>/css/datetime-picker.css" rel="stylesheet">
	<script type="text/javascript">
		var j211 = $.noConflict(true);
	</script>
	<!-- Datetime picker -->
	
	<!--/*Tag suggest by Hanh*/-->
	<script src="<?php echo bloginfo('template_directory');?>/js/jquery-1.9.0.js"></script>
	<script src="<?php echo bloginfo('template_directory');?>/js/jquery-ui-1.10.0.custom.js"></script>
	<script src="<?php echo bloginfo('template_directory');?>/js/jquery.taghandler.js"></script>
	<link type="text/css" href="<?php echo bloginfo('template_directory');?>/css/jquery.taghandler.min.css" rel="stylesheet">
	<link type="text/css" href="<?php echo bloginfo('template_directory');?>/css/jquery-ui.css" rel="stylesheet">
	<link type="text/css" href="<?php echo bloginfo('template_directory');?>/css/prettify.css" rel="stylesheet">	
	<script type="text/javascript">
		 var j190 = $.noConflict(true);
	</script>		
	<!--/*Tag suggest by Hanh*/-->
	
	<!--slider-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo bloginfo('template_directory');?>/css/camera.css"/>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo bloginfo('template_directory');?>/css/style_camera.css"/>
	<!--slider-->
  </head>
<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3&appId=145938085600421";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="parallax-bg"></div>
<!--<marquee behavior="scroll" direction="right">-->
<header id="masthead" class="site-header" role="banner">
<?php if ( get_theme_mod( 'itek_tagline_visibility' ) != 0 ) { ?>
    <div class="container">
	   <h3 class="site-description"><span class="site-description-content"><?php bloginfo( 'description' ); ?></span><img src="<?php echo bloginfo('template_directory');?>/img/day.png" style="float:left;"><img src="<?php echo bloginfo('template_directory');?>/img/plane.gif" style="float:left;"></h3>
   </div>
<?php } ?>
</header> <!--/#header--><!--</marquee>-->
<?php do_action( 'itek_before_top_nav' ); ?>
<?php get_template_part( 'nav-top' ); ?>
<?php do_action( 'itek_after_top_nav' ); ?>

<div class="clearfix"></div>
<div id="page" class="hfeed site">
<div class="expandable-image"></div>
<?php 
do_action( 'itek_before_secondary' );
if ( has_nav_menu( 'secondary' ) || has_nav_menu( 'social' ) ) : // Check if there's a menu assigned to either the 'secondary' or 'social' locations. ?>
   <div class="clearfix"></div>
   <?php get_template_part( 'nav-sec' ); 
endif; // End check for menu.
do_action( 'itek_after_secondary' );
?>

<?php if ( is_front_page() ) : ?>
<div class="container">
<div id="showcase">
	<?php do_action( 'itek_before_in_showcase' ); ?>
        <?php get_sidebar( 'showcase' ); ?>
	<?php do_action( 'itek_after_in_showcase' ); ?>
</div>
</div>
<?php endif; ?>

<div class="container">
<?php
global $woocommerce;
$assigned=array();
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) { 
	$_product = $values['data']->post; 
	array_push($assigned,"'".$_product->post_title."'");
	//echo $_product->post_title; 
} 
$assignedTags = implode(",",$assigned);
	 $args = array(
            'posts_per_page' => -1,         
            'post_type' => 'product',
            'orderby' => 'title,'
        );
	$html=array();
	$products = new WP_Query( $args );
	while ( $products->have_posts() ) {
            $products->the_post();
		array_push($html,"'".get_the_title()."'");
	}
	$array_tags = implode(",",$html);
?>
<script type="text/javascript">
     j190(function(){
          j190(".methodExample").tagHandler({
               assignedTags: [ <?php echo $assignedTags; ?> ],								            
               availableTags: [ <?php echo $array_tags; ?> ],
               autocomplete: true,
			   allowAdd: false,
               afterAdd: function() {document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n"));},
               afterDelete: function() {document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n"));},
			   afterLoad : function() { document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n")); }
        });																
               prettyPrint();
   })
</script>
<script>
    function swapContent(page) {
		jQuery( ".methodExample" ).load(page);
		j190(".methodExample").tagHandler({
               assignedTags: [],								            
               availableTags: [ <?php echo $array_tags; ?> ],
               autocomplete: true,
			   allowAdd: false,
               afterAdd: function() {document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n"));},
               afterDelete: function() {document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n"));},
			   afterLoad : function() { document.getElementById("mytag").value =(j190("#example_get_tags").tagHandler("getTags").join("\n")); }
        });																
               prettyPrint();
   }
</script>